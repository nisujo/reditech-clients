const express = require('express');
const cors = require('cors');
require('dotenv').config();
const { Connection } = require('./database/connection');

require('./database/connection');

const app = express();

app.use(cors());
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Settings
app.set('port', process.env.PORT || 3000);

// Routes
app.use(require('./routes/home'));
app.use(require('./routes/login'));
app.use(require('./routes/clients'));

// Connection.getInstance();

// Start
app.listen(app.get('port'), () => {

    console.log(`Server listening on port: ${app.get('port')}`);

});