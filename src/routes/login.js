const { Router } = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { Connection } = require('../database/connection');

const router = Router();

router.post('/login', (req, res) => {
    const email = req.body.email;
    const connection = Connection.getInstance().connection;
    const emailEscaped = connection.escape(email);
    const query = `select * from users where email = ${emailEscaped}`;

    Connection.executeQuery(query, (err, results) => {

        if (err) {
            return res.status(401).json({ err, message: 'Invalid email or password' });
        } else if (results.length == 0) {
            return res.status(401).json({ err, message: 'Invalid email or password' });
        } else if (!bcrypt.compareSync(req.body.password, results[0].password)) {
            return res.status(401).json({ err, message: 'Invalid email or password' });
        }

        const user = results[0];

        let token = jwt.sign({
            user
        }, process.env.JWT_SEED, {
            expiresIn: process.env.JWT_EXPIRE
        });

        res.json({
            token,
            user: {
                id: user.id,
                name: user.name,
                email: user.email,
            }
        });

    });

});

module.exports = router;