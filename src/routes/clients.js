const { Router } = require('express');
const { Connection } = require('../database/connection');
const { formatTimeStamp } = require('../utils/utils');
const { validateToken } = require('../middlewares/authentication');

const router = Router();

router.get('/clients', validateToken, (req, res) => {
    const query = `select * from clients`;

    Connection.executeQuery(query, (err, results) => {

        if (err) return res.status(500).json({ err, message: 'Internal database error' });

        res.json({ clients: results });

    });
});

router.post('/clients', validateToken, (req, res) => {
    const name = req.body.name;
    const phone = req.body.phone;
    const address = req.body.address;
    const created_at = formatTimeStamp(new Date());

    if (!name || !phone || !address) {
        return res.status(403).json({ message: 'Name, phone and address are required.' });
    }

    if (!name.trim() || !phone.trim() || !address.trim()) {
        return res.status(403).json({ message: 'Name, phone and address can\'t be empty.' });
    }

    const connection = Connection.getInstance().connection;

    const nameEscaped = connection.escape(name.trim());
    const phoneEscaped = connection.escape(phone.trim());
    const addressEscaped = connection.escape(address.trim());

    const query = `
        INSERT INTO clients(name, phone, address, created_at, updated_at)
        VALUES (${nameEscaped}, ${phoneEscaped}, ${addressEscaped}, '${created_at}', '${created_at}')
    `;

    Connection.executeQuery(query, (err, results) => {

        if (err) return res.status(500).json({ err, message: 'Internal database error' });

        const client = {
            id: results.insertId,
            name,
            phone,
            address,
            created_at,
            updated_at: created_at,
        }

        res.json({
            message: 'Client successfully saved.',
            client,
        });

    });
});

router.get('/clients/:id', validateToken, (req, res) => {
    const id = req.params.id;
    const idEscaped = Connection.getInstance().connection.escape(id);
    const query = `select * from clients where id = ${idEscaped}`;

    Connection.executeQuery(query, (err, results) => {

        if (err) return res.status(500).json({ err, message: 'Internal database error' });

        res.json({ client: results[0] });

    });
});

router.put('/clients/:id', validateToken, (req, res) => {
    const id = req.params.id;
    const name = req.body.name;
    const phone = req.body.phone;
    const address = req.body.address;
    const updated_at = formatTimeStamp(new Date());

    if (!name || !phone || !address) {
        return res.status(403).json({ message: 'Name, phone and address are required.' });
    }

    if (!name.trim() || !phone.trim() || !address.trim()) {
        return res.status(403).json({ message: 'Name, phone and address can\'t be empty.' });
    }

    const connection = Connection.getInstance().connection;

    const idEscaped = connection.escape(id);
    const nameEscaped = connection.escape(name.trim());
    const phoneEscaped = connection.escape(phone.trim());
    const addressEscaped = connection.escape(address.trim());

    const query = `
        UPDATE clients
        SET
            name=${nameEscaped},
            phone=${phoneEscaped},
            address=${addressEscaped},
            updated_at='${updated_at}'
        where id=${idEscaped}
    `;

    Connection.executeQuery(query, (err, results) => {

        if (err) return res.status(500).json({ err, message: 'Internal database error' });
        if (results.affectedRows == 0) return res.status(404).json({ message: 'No client where updated' });

        const client = {
            id,
            name,
            phone,
            address,
            updated_at,
        }

        res.json({
            message: 'Client successfully updated.',
            client,
            results,
        });

    });
});

router.delete('/clients/:id', validateToken, (req, res) => {
    const id = req.params.id;
    const idEscaped = Connection.getInstance().connection.escape(id);
    const query = `DELETE FROM clients WHERE id = ${idEscaped}`;

    Connection.executeQuery(query, (err, results) => {

        if (err) return res.status(500).json({ err, message: 'Internal database error' });
        if (results.affectedRows == 0) return res.status(404).json({ message: 'No client where deleted' });

        res.json({
            message: 'Client successfully deleted.',
            results,
        });

    });
});

module.exports = router;