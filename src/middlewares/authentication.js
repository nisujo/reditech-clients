const jwt = require('jsonwebtoken');

let validateToken = (req, res, next) => {
    let token = req.get('token');

    jwt.verify(token, process.env.JWT_SEED, (err, decoded) => {

        if (err) return res.status(401).json({
            message: 'Access not authorized',
            err,
        });

        req.user = decoded.user;
        next();

    });
}

module.exports = { validateToken };