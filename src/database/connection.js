const mysql = require('mysql');

class Connection {

    constructor() {
        this.connection = mysql.createConnection({
            host: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_DATABASE,
        });
    }

    connectDatabase() {
        this.connection.connect((err) => {
            if (err) {
                console.log(err.message);
                return;
            }
        });
    }

    destroyConnection() {
        if (this.connection) this.connection.destroy();
    }

    static getInstance() {
        return new Connection();
    }

    static executeQuery(query, callback) {
        const conn = new Connection();
        conn.connectDatabase();

        conn.connection.query(query, (err, results, fields) => {

            conn.destroyConnection();

            if (err) {
                console.log(err);
                return callback(err);
            }

            if (results.length === 0) {
                return callback('No results found');
            }

            callback(null, results);

        });
    }

}

/**
 * Reemplazo de código de conexión a base de datos
 * como parche a error (protocol_enqueue_after_fatal_error).
 */

// class Connection {
//     static _instance = null;

//     constructor() {
//         console.log('Start DB connection');

//         this.connection = mysql.createConnection({
//             host: process.env.DB_HOST,
//             user: process.env.DB_USER,
//             password: process.env.DB_PASSWORD,
//             database: process.env.DB_DATABASE,
//         });

//         this.connectDatabase();
//     }

//     static getInstance() {
//         return this._instance || (this._instance = new Connection());
//     }

//     connectDatabase() {
//         this.connection.connect((err) => {
//             if (err) {
//                 console.log(err.message);
//                 return;
//             }

//             console.log('Database online');
//         });
//     }

//     static executeQuery(query, callback) {
//         this._instance.connection.query(query, (err, results, fields) => {

//             if (err) {
//                 console.log(err);
//                 return callback(err);
//             }

//             if (results.length === 0) {
//                 return callback('No results found');
//             }

//             callback(null, results);

//         });
//     }
// }

module.exports = { Connection }